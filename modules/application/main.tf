
resource "kubectl_manifest" "forecastle_app" {
  depends_on = [var.depends_list]
  yaml_body  = <<YAML
apiVersion: forecastle.stakater.com/v1alpha1
kind: ForecastleApp
metadata:
  name: ${var.name}
  namespace: ${var.namespace}
spec:
  name: ${var.readable_name}
  group: ${var.group}
  icon: ${var.icon}
  url: ${var.url}
  YAML
}
