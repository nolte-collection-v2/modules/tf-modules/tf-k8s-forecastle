variable "namespace" {
}
variable "readable_name" {

}
variable "name" {

}
variable "group" {
  default = "unsort"
}
variable "url" {
  default = "http://app-url"
}
variable "icon" {
  default = "https://icon-url"
}
variable "depends_list" {
  default = []
}
