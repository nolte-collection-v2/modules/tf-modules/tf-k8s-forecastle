
locals {
  EXTRA_VALUES = {
    forecastle = {
      createCustomResource = "false"
      config = {
        crdEnabled = true
        namespaceSelector = {
          any = true
        }
      }
    }
  }
}

locals {
  ingress_forecastle_annotations = {
    "forecastle.stakater.com/appName" : "Forecastle",
    "forecastle.stakater.com/group" : "utils",
    "forecastle.stakater.com/icon" : "https://raw.githubusercontent.com/stakater/Forecastle/master/assets/web/forecastle-round-100px.png",
  }
}

locals {
  ingress_annotations = {
    forecastle = {
      ingress = {
        annotations = local.ingress_forecastle_annotations
      }
    }
  }
}



resource "helm_release" "release" {
  name       = "forecastle"
  repository = "https://stakater.github.io/stakater-charts"
  chart      = "forecastle"
  namespace  = var.namespace
  values = [
    yamlencode(local.ingress_annotations),
    yamlencode(local.EXTRA_VALUES),
    yamlencode(var.extra_values),
  ]

  skip_crds = true
  set {
    name  = "forecastle.namespace"
    value = var.namespace
  }
  set {
    name  = "forecastle.ingress.enabled"
    value = "true"
  }

}
