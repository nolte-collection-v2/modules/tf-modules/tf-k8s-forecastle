variable "forecastle_app_operator_version" {
  default = "v1.0.59"
}

variable "depends_list" {
  default = []
}
