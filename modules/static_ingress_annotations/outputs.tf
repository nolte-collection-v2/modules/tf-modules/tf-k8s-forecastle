output "annotations" {
  value = {
    "forecastle.stakater.com/appName" : "Forecastle",
    "forecastle.stakater.com/group" : "utils",
    "forecastle.stakater.com/icon" : "https://raw.githubusercontent.com/stakater/Forecastle/master/assets/web/forecastle-round-100px.png",
  }
}
